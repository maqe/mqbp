```
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMXOkkkkkkKWMMMMMMMMWKkkkkkkOXMMMMMMMMMMMXOkkkkkkKWMMMMMMMMMMMMMMMWNK0OkkkO0XWMMMMMMMMWKOkkkkkkkkkkkkkkkkkkkOXMMMMMM
MMMMMMO;'''''':kNMMMMMMNk:'''''';kMMMMMMMMMMKl'''''''c0MMMMMMMMMMMMWXkoc;,''''',;cd0NMMMMMNd'''''''''''''''''''',kWMMMMM
MMMMMMO;''''''',l0WMMWKo,''''''';kMMMMMMMMMNo,''''''''lXMMMMMMMMMWXx:'''''''''''''',lONMMMNd'''''''''''''''''''',kWMMMMM
MMMMMMO;''''''''';xXNk:''''''''';kMMMMMMMMWx,''''''''',dNMMMMMMMW0c'''''''',,,''''''',dXMMNd''''''',;;;;;;;;;;;;:OWMMMMM
MMMMMMO;'''''''''''cxl,''''''''';kMMMMMMMWO;''''''''''';kWMMMMMM0:'''''':dO0K0ko;''''',oNMWKl,''''';d0XXXXXXXXXXXWMMMMMM
MMMMMMO;'''''',''''';ll;','''''';kMMMMMMM0c'''''',,''''':0WMMMMNo''''',oXMMMMMMWO:''''';OWMMNk:''''',oKWMMMMMMMMMMMMMMMM
MMMMMMO;''''':xo,''''':okx:''''';kMMMMMMXl''''''ckl''''''lKMMMMKc'''''cKMMMMMMMMWx,'''',dWMMMWKo;''''':0WMMMMMMMMMMMMMMM
MMMMMMO;'''''cKNkollllo0W0:''''';kMMMMMNd,''''';OW0:''''''oNMMMKc''''':OWMMMMMMMNd,'''',xWMMMNkooc,'';dXMMMMMMMMMMMMMMMM
MMMMMMO;'''''cKMMWWWWWMMM0:''''';kWMMMWk;''''',dNMWk;''''',xXWMWx,'''''ckNWMMMWKd;'''''c0MMW0l,';ll:l0WMMMMMMMMMMMMMMMMM
MMMMMMO;'''''cKMMMMMMMMMM0:''''';kMMMWO:'''''':kKkkd;'''''':lOWMXo,''''',:ok00x:'''''':OWMWO;''''':ldxxxxxxxxxxxkXMMMMMM
MMMMMMO;'''''cKMMMMMMMMMM0:''''';kMMMKc''''''',oOd;''''''''''cKMMXx;'''''''',:cccc:,,l0WMMNd'''''''''''''''''''',kWMMMMM
MMMMMMO;'''''cKMMMMMMMMMM0:''''';kWMXo''''''';oKWW0l,'''''''''oXMMWKd:,''''''''',:ccdXMMMMNd'''''''''''''''''''',kWMMMMM
MMMMMM0c:;;::oKMMMMMMMMMMKo:::::c0WWOc;;;;;;lOWMMMMNOl;;;;:;;;ckWMMMWXOdl;'''''''''',cdKWMWx::::::::::::::::::::cOMMMMMM
MMMMMMWNXXXXXNWMMMMMMMMMMWNNNNNXNWMMNXXXXXXXNMMMMMMMMNXXXXXXXXXNMMMMMMMMWXOdl;''''''';dXMMMNXXXXXXXXXXXXXXXXXXXXNWMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWXOdl;',l0WMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKOONMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
```

# MAQE Boilerplate

See CHANGELOG for version notes.

To see this repo used inside a sample project please refer to the MAQE [New Project Template](https://bitbucket.org/maqe/mqbp-project-template) repo.

---

## Development setup

Install dependencies if not present:
```
npm install bower
```

Development install:
```
npm install
bower install
```

Development build:
```
gulp modernizr
gulp build:debug
```

Continuous build:
```
gulp watch
```

## Production setup

Production install and build:
```
npm install --quiet
bower install --quiet
gulp build:production

```

Production build with sourcemap files:
```
gulp build:production --sourcemaps

```

If the app deployment does not support server asset building then make sure to comment out these lines in the ```.gitignore```:
```
public/assets/css/app*
public/assets/js/app*
public/assets/js/vendor*
public/assets/rev-manifest*
```

## App config file

The build system assumes and requires an ```app-config.json``` file located on the root of your project, which it will automatically use for all gulp tasks. A sample config file is included in this repo. (For an example of how this repo is used in an actual project please refer to the [New Project Template](https://bitbucket.org/maqe/mqbp-project-template) repo.)

In cases where a project may have or require multiple app front-ends you can specify a custom app config file using the following format:

```gulp build:debug --config=app-config-custom.json```
or
```gulp build:debug -c=app-config-custom.json```

You may also set the location of the app config file in an ENV variable named ```GULP_BUILD_APP_CONFIG```.

## Deployment

### Via SSH

Build production assets and deploy via SSH to a server:
```
npm install --quiet
bower install --quiet
gulp deploy:scp --target=ENV
```

You can also use the short form for target ```-t=ENV```.

Requires an SSH private key and server config in the ```config/scp.json``` file. Target ENV is used to select which data from the ```scp.json``` config.

Note: You still need to do a ```git ftp push -s ENV``` before deploying.

### To a CDN S3

If the project uses a CDN (S3, Cloudfront, etc.) to distribute the build assets then you do not need to do a full build and scp deploy to the UAT or Production server. You can use an abbreviated upload which only sends the ```rev-manifest.json``` file, which is required for the correct cache-busting - using ```gulp deploy:scp --target=ENV --rev``` or ```gulp deploy:scp -t=ENV -r```. In some deployment setups the revision manifest may be generated on the server or injected via deployment; it all depends on your project setup.

Build production assets and deploy to an S3 bucket:
```
npm install --quiet
bower install --quiet
gulp deploy:s3 --target=ENV
```

You can also use the short form for target ```-t=ENV```.

Requires AWS credentials and S3 bucket info in the ```config/aws.json``` file. Target ENV is used to select which data from the ```aws.json``` config.

S3 upload is set up to work as the origin for a Cloudfront CDN.

### To S3 for Opsworks

If the server infrastructure is managed by AWS Opsworks a custom deploy task must be used to get the correct version of the asset ```rev-manifest.json``` file into S3 for Opsworks to download and use. The build __must__ be run from inside a git repo so that a valid revision hash can be generated.

```gulp deploy:s3ops --target=ENV```

NOTE: This build should __only__ be triggered automatically, because in some cases a manual trigger could have asset file name mismatch with the git hash revision versioining of the rev-manifest file.


