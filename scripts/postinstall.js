/**
 * Setup symlinks to be able to access files and run gulp from project root
 */
const fs = require("fs-extra");

function symlink(path, target) {
	try {
		// Create symlink if file doesn't exist
		fs.symlinkSync(target, path);
	} catch (err) {
		// If file exist, do nothing.
	}
}

// Symlink files
const symlinkFiles = new Map([
	["./node_modules/maqe-boilerplate/less/", "./../../mqbp"],
	["./node_modules/maqe-boilerplate/.babelrc", "./../../.babelrc"],
	["./node_modules/maqe-boilerplate/gulpfile.js", "./../../gulpfile.js"]
]);

symlinkFiles.forEach((value, key) => symlink(value, key));

// Copy files
// .eslintrc & .stylelintrc
const copyFiles = new Map([
	["./.eslintrc", "./../../.eslintrc"],
	["./.stylelintrc", "./../../.stylelintrc"]
]);

copyFiles.forEach((target, src) => {
	fs.copy(src, target, { overwrite: false },
		(err) => {
			if (err) {
				console.log(err);
			}
		}
	);
});