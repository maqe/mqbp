module.exports = (gulp, config) => {
	const gulpif = require("gulp-if");
	const concat = require("gulp-concat");
	const less = require("gulp-less");
	const postcss = require("gulp-postcss");
	const stylelint = require("gulp-stylelint");
	const autoprefixer = require("gulp-autoprefixer");
	const pleeease = require("gulp-pleeease");
	const sourcemaps = require("gulp-sourcemaps");

	// LESS linting
	gulp.task("stylelint", () => {
		return gulp.src(config.assets.lint.less)
			// Stylelint
			.pipe(stylelint({
				failAfterError: !config.watch,
				reporters: [{
					formatter: "string", console: true
				}]
			}));
	});

	// LESS
	gulp.task("less", () => {
		return gulp.src(config.assets.entries.less)
			.pipe(gulpif(config.debug, sourcemaps.init()))
			.pipe(less({}))
			.on("error", (err) => {
				console.log(err.message);
			})
			.pipe(autoprefixer({
				browsers: config.options.autoprefixer,
				cascade: false
			}))
			// postcss font path based on fonts.less and assets/fonts folder
			.pipe(postcss([require("postcss-fontpath")]))
			.pipe(gulpif(config.debug, sourcemaps.write()))
			.pipe(gulp.dest(".tmp/assets/css"));
	});

	// Main styles LESS tasks
	gulp.task("styles", ["stylelint", "less"], () => {
		return gulp.src(config.assets.cssFiles)
			.pipe(gulpif(config.debug, sourcemaps.init({ loadMaps: true })))
			.pipe(gulpif(config.debug, concat("app-debug.css"), concat("app.css")))
			.pipe(gulpif(config.debug, sourcemaps.write()))
			.pipe(gulpif(!config.debug, pleeease({
				autoprefixer: false,
				filters: false,
				rem: false,
				pseudoElements: false,
				opacity: false,
				import: false,
				mqpacker: false
			})))
			.pipe(gulp.dest(`${config.assets.dest}/css`));
	});
};
