module.exports = (gulp, config) => {
	const gulpif = require("gulp-if");
	const eslint = require("gulp-eslint");
	const browserify = require("browserify");
	const debowerify = require("debowerify");
	const watchify = require("watchify");
	const babelify = require("babelify");
	// NOTE: Uncomment this if you are doing Redux.
	// Also add "loose-envify" package in your package.json.
	// const envify = require("loose-envify/custom");
	const source = require("vinyl-source-stream");
	const buffer = require("vinyl-buffer");
	const sourcemaps = require("gulp-sourcemaps");
	const uglify = require("gulp-uglify");
	const log = require("fancy-log");
	const runSequence = require("run-sequence");
	const argv = require("yargs").argv;
	const needSourcemaps = argv.sourcemaps || argv.maps || false;

	// Type of JS file.
	const appType = {
		app: "app",
		vendor: "vendor"
	};

	/**
	 * All Browserify transforms for both app and vendor.
	 * @param {Browserify} bundler
	 * @param {string} type Type of JS file. "app" or "vendor"
	 */
	function browserifyTransforms(bundler, type = appType.app) {
		// Transform for both "app" and "vendor"

		// NOTE: Envify required for Redux app. Uncomment this if you are doing Redux.
		// bundler.transform(envify({
		// 	NODE_ENV: config.nodeEnv
		// }), {
		// 	global: true
		// });

		// Transform for only "app"
		if (type === appType.app) {
			bundler.transform(babelify);
		}

		// Transform for only "vendor"
		if (type === appType.vendor) {
			bundler.transform(debowerify);
		}
	}

	/**
	 * Bundle JS files, doing sourcemaps, uglify and public it.
	 * @param {Browserify} bundler
	 * @param {string} type Type of JS file. "app" or "vendor"
	 */
	function doBundle(bundler, type = appType.app) {
		const filenameDebug = `${type}-debug.js`;
		const filenameDist = `${type}.js`;

		const ifAppAndDebugOrNeedSourcemaps = (type === appType.app && (config.debug || needSourcemaps));

		return bundler
			.bundle() // Bundle the files into a single javascript file.
			.pipe(gulpif(config.debug, source(filenameDebug), source(filenameDist))) // Get source based on debug.
			.pipe(buffer())
			.pipe(gulpif(ifAppAndDebugOrNeedSourcemaps, sourcemaps.init({ loadMaps: true }))) // Loads source map from Browserify files.
			.pipe(gulpif(!config.debug, uglify())) // Only uglify if not a debug build.
			.pipe(gulpif(ifAppAndDebugOrNeedSourcemaps, sourcemaps.write("./"))) // Write sourcemap file.
			.pipe(gulp.dest(`${config.assets.dest}/js`));
	}

	/**
	 * Initialize Browserify instance with JS files.
	 * @return {Browserify}
	 */
	function initialBrowserify() {
		// Get Browserify options.
		const options = Object.assign({}, watchify.args, {
			entries: config.assets.entries.js,
			extensions: config.assets.browserify.extensions,
			debug: config.debug || needSourcemaps
		});

		// Add Watchify plugin if called from watch.
		if (config.watch) {
			options.plugin = [watchify];
		}

		// Initialize Browserify.
		const bundler = browserify(options);

		// Set event handler.
		bundler
			.on("error", log.bind(log, "Browserify Error"))
			.on("log", log) // Fires after a bundle was created on watch.
			.on("update", () => {
				// Run "eslint" and bundle file again on watch.
				runSequence(
					"eslint",
					doBundle.bind(null, bundler)
				);
			});

		// Prevent external vendor files from being loaded into the current bundle.
		bundler.external(config.assets.browserify.externalVendors);

		// Do all transform.
		browserifyTransforms(bundler, appType.app);

		// Return Browserify instance.
		return bundler;
	}

	/**
	 * ESLint task
	 */
	gulp.task("eslint", () => (
		gulp.src(config.assets.lint.js)
			.pipe(eslint())
			// Display report
			.pipe(eslint.format())
			// Not throw error on watch.
			.pipe(gulpif(!config.watch, eslint.failAfterError()))
	));

	/**
	 * Build production JS file.
	 */
	gulp.task("scripts", ["eslint"], () => doBundle(initialBrowserify(), appType.app));

	/**
	 * Build debug JS file.
	 */
	gulp.task("scripts:debug", ["set-debug", "eslint"], () => doBundle(initialBrowserify(), appType.app));

	/**
	 * Build vendor JS files.
	 */
	gulp.task("scripts-vendor", () => {
		// Initialize Browserify.
		const bundler = browserify(config.assets.entries.vendorJs);

		// Do all transform.
		browserifyTransforms(bundler, appType.vendor);

		// Explicitly require all vendor files listed in app-config, this coincides with the external list during app bundle
		bundler.require(config.assets.browserify.externalVendors);

		return doBundle(bundler, appType.vendor);
	});

	/**
	 * Watch task for JS.
	 */
	gulp.task("watchify:debug", (cb) => {
		runSequence(
			"set-watch",
			"scripts:debug",
			cb
		);
	});
};
