module.exports = (gulp, config) => {
	const exec = require("child_process").exec;
	const del = require("del");

	// Clean up all tmp and built assets files
	gulp.task("clean", () =>
		del([
			"./.tmp",
			`${config.assets.dest}/css/*.css`,
			`${config.assets.dest}/js/*.js`,
			`${config.assets.dest}/js/*.js.map`,
			`${config.assets.dest}/**/rev-manifest*`
		])
	);

	// Get git username for use in notifications mainly
	gulp.task("username", (cb) => {
		exec("git config user.name", (err, stdout) => {
			if (config.project) {
				config.project.username = stdout;
			}
			cb(err);
		});
	});

	// Get git revision hash for use in s3 opsworks deploys
	gulp.task("git-hash", (cb) => {
		exec("git rev-parse HEAD", (err, stdout) => {
			if (config.project) {
				config.project.s3hash = stdout.trim();
			}
			cb(err);
		});
	});

	// A task to set debug flag to true,
	// needs to be called first before any tasks that require debug to be true
	gulp.task("set-debug", (cb) => {
		config.debug = true;
		config.nodeEnv = "development";
		cb();
	});

	// A task to set watch flag to true,
	// needs to be called first before any tasks that require watch to be true
	gulp.task("set-watch", (cb) => {
		config.watch = true;
		cb();
	});
};
