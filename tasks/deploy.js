module.exports = (gulp, config) => {
	// tasks dependencies:
	//		utils.js
	//		build.js

	const fs = require("fs");
	const log = require("fancy-log");
	const PluginError = require("plugin-error");
	const argv = require("yargs").argv;
	const rename = require("gulp-rename");
	const parallelize = require("concurrent-transform");
	const awspublish = require("gulp-awspublish");
	const scp = require("gulp-scp2");
	const ftp = require("vinyl-ftp");
	const HipChatNotify = require("hipchat-notify");

	const DEPLOYMENT_TYPE = {
		SCP: "scp",
		S3: "s3",
		OPSWORKS: "opsworks"
	};

	const ERROR_MSG = {
		TARGET_MISSING: `Error: Deploy environment is missing. Do you forgot "--target=ENV" ?`,
		HASH_MISSING: "Error: No valid git revision hash."
	};

	/**
	 * Function to generate Hipchat message notify after deployed code.
	 * @param {string} deployMethod - Deployment method. Possible values are "s3", "opsworks", "scp".
	 * @param {string} targetBranch - Branch to deploy.
	 * @param {string} targetDomain - Domain to deploy to.
	 * @param {string} targetBucket - S3 Bucket to deploy to. Only for "s3" & "opsworks" method.
	 */
	function generateHipchatMsg(deployMethod, targetBranch, targetDomain, targetBucket) {
		const domainElement = `<a href="http://${targetDomain}"><b>${targetDomain}</b></a>`;
		let msg = `<b>${config.project.name}</b> asset build successfully deployed to <b>${targetBranch}</b> `;

		switch (deployMethod) {
			case DEPLOYMENT_TYPE.SCP:
				msg += `at ${domainElement} via SSH`;
				break;

			case DEPLOYMENT_TYPE.S3:
				msg += `bucket <b>${targetBucket}</b> for ${domainElement} via S3`;
				break;

			case DEPLOYMENT_TYPE.OPSWORKS:
				msg += `bucket <b>${targetBucket}</b> for ${domainElement} via S3 for Opsworks`;
				break;

			default:
				return "";
		}

		if (!!config.project.username && config.project.username !== "") {
			msg += ` by <b>${config.project.username}</b>`;
		}

		return msg;
	}

	/**
	 * Initialize HipChat Notification.
	 * @return {HipChatNotify}
	 */
	function initialHipChatNotification() {
		const hipchatConfig = JSON.parse(fs.readFileSync(config.configs.hipchat, "utf8"));
		if (hipchatConfig.roomGit) {
			return new HipChatNotify(hipchatConfig.roomGit.ID, hipchatConfig.roomGit.token);
		}

		return null;
	}

	/**
	 * Deployment function for S3
	 * @param {string} deployType Deploy type. "opsworks" or "s3".
	 * @param {string} targetEnv Target environment.
	 * @param {string} hash Git hash for append to rev-manifest file, use only in "opsworks" deploy.
	 * @return {stream}
	 */
	function deployS3(deployType, targetEnv, hash = undefined) {
		const awsConfig = JSON.parse(fs.readFileSync(config.configs.aws, "utf8"));

		// Throw error if S3 config is not exists.
		if (!awsConfig[targetEnv] || !awsConfig[targetEnv].s3) {
			throw new PluginError(`deploy:${deployType}`, `Error: S3 config is missing in ${config.configs.aws}.`);
		}

		// Read AWS config.
		const credentials = awsConfig[targetEnv].s3;
		const headers = awsConfig.headers;
		const publisher = awspublish.create(credentials);

		const hipchat = initialHipChatNotification();

		// If deploy Opsworks, append Git hash to manifest.
		if (deployType === DEPLOYMENT_TYPE.OPSWORKS && hash !== undefined) {
			// Generate revisioned rev-manifest file for opsworks using git hash.
			// Get file name.
			const oldFile = config.options.deploy.manifest;
			// Append git hash to filename.
			const newFile = `${oldFile.substr(0, oldFile.lastIndexOf("."))}-${hash}.json`;
			fs.renameSync(oldFile, newFile);
		}

		return gulp.src(config.options.deploy.s3.files)
			.pipe(rename((path) => {
				path.dirname = `/assets/${path.dirname}`;
			}))
			.pipe(awspublish.gzip())
			.pipe(parallelize(publisher.publish(headers, {}), 50))
			.pipe(awspublish.reporter())
			.on("error", (err) => {
				console.log(err);

				if (hipchat) {
					hipchat.error(`Asset build deploy to ENV ${targetEnv} failed.`);
				}
			})
			.on("finish", () => {
				console.log(">> deploy finished");

				if (hipchat) {
					hipchat.notify({
						message: generateHipchatMsg(deployType, targetEnv, awsConfig[targetEnv].domain, credentials.params.Bucket),
						color: "purple",
						notify: false
					});
				}
			});
	}

	/**
	 * Call formats:
	 * gulp deploy:s3 --target=ENV
	 * gulp deploy:s3 -t=ENV
	 */
	gulp.task("deploy:s3", ["build:production", "username"], () => {
		// Deploy to s3 using credentials based on ENV
		const target = argv.target || argv.t;

		if (target === undefined) {
			throw new PluginError("deploy:s3", ERROR_MSG.TARGET_MISSING);
		}

		return deployS3(DEPLOYMENT_TYPE.S3, target);
	});

	/**
	 * Build command specifically for deploying to S3 in preparation for an opsworks setup
	 * 	for automated deploys to multiple servers (i.e. opsworks) we need to make sure the rev-manifest.json file is available on all servers
	 * 	but since that file is not versioned we need to help get a versioned copy of it to a common location (i.e. S3) so each server can retreive the correct version
	 *
	 * Call formats:
	 * 	gulp deploy:s3ops --target=ENV
	 * 	gulp deploy:s3ops -t=ENV
	 */
	gulp.task("deploy:s3ops", ["build:production", "username", "git-hash"], () => {
		// Deploy to s3 using credentials based on ENV
		const target = argv.target || argv.t;
		const hash = config.project.s3hash;

		if (!hash) {
			throw new PluginError("deploy:s3ops", ERROR_MSG.HASH_MISSING);
		}

		if (!target) {
			throw new PluginError("deploy:s3ops", ERROR_MSG.TARGET_MISSING);
		}

		return deployS3(DEPLOYMENT_TYPE.OPSWORKS, target, hash);
	});

	/**
	 * Deploy via SSH
	 *
	 * Call formats:
	 *		gulp deploy:scp --target=ENV
	 *		gulp deploy:scp -t=ENV
	 * If using a CDN (s3/cloudfront) then only need to upload the rev-manifest.json to the actual server
	 *		gulp deploy:scp --target=ENV --rev
	 *		gulp deploy:scp -t=ENV -r
	 */
	gulp.task("deploy:scp", ["build:production", "username"], () => {
		const target = argv.target || argv.t;
		if (!target) {
			throw new PluginError("deploy:scp", ERROR_MSG.TARGET_MISSING);
		}

		const scpConfig = JSON.parse(fs.readFileSync(config.configs.scp, "utf8"));
		const host = scpConfig[target].host;
		const username = scpConfig[target].username;
		const dest = scpConfig[target].dest;

		const hipchat = initialHipChatNotification();
		const key = fs.readFileSync(config.options.deploy.scp.key);

		const files = (argv.r) ? config.options.deploy.manifest : config.options.deploy.scp.files;

		return gulp.src(files)
			.pipe(scp({
				host,
				username,
				dest,
				privateKey: key,
				watch(client) {
					client.on("write", (o) => {
						console.log(">> write %s", o.destination);
					});
				}
			}))
			.on("error", (err) => {
				console.log(err);
			})
			.on("finish", () => {
				console.log(">> deploy finished");
				if (hipchat) {
					hipchat.notify({
						message: generateHipchatMsg("scp", target, scpConfig[target].domain),
						color: "purple",
						notify: false
					});
				}
			});
	});

	/**
	 * Deploy via FTP
	 *
	 * Call formats:
	 *		gulp deploy:ftp --target=ENV
	 *		gulp deploy:ftp -t=ENV
	 * If using a CDN (s3/cloudfront) then only need to upload the rev-manifest.json to the actual server
	 *		gulp deploy:ftp --target=ENV --rev
	 *		gulp deploy:ftp -t=ENV -r
	 */
	gulp.task("deploy:ftp", ["build:production", "username"], () => {
		const target = argv.target || argv.t;

		if (!target) {
			throw new PluginError("deploy:ftp", ERROR_MSG.TARGET_MISSING);
		}

		const ftpConfig = JSON.parse(fs.readFileSync(config.configs.ftp, "utf8"));
		const host = ftpConfig[target].host;
		const username = ftpConfig[target].username;
		const password = ftpConfig[target].password;
		const dest = ftpConfig[target].dest;
		const files = (argv.r) ? config.options.deploy.manifest : config.options.deploy.ftp.files;

		const conn = ftp.create({
			host,
			user: username,
			password,
			log
		});

		return gulp.src(files)
			.pipe(conn.dest(dest))
			.on("error", (err) => {
				console.log(err);
			});
	});
};
