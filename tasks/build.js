module.exports = (gulp, config) => {
	// tasks dependencies:
	//		utils.js
	//		scripts.js
	//		styles.js
	//		modernizr.js

	const fs = require("fs-extra");
	const runSequence = require("run-sequence");
	const through = require("through2");
	const rev = require("gulp-rev");

	/**
	 * Remove original asset files after append revision hash
	 */
	function removeOriginalAssets() {
		return through.obj((file, enc, cb) => {
			if (file.revOrigPath) {
				// Remove files
				fs.remove(file.revOrigPath, (err) => {
					cb(err, file);
				});
			} else {
				// Nothing to remove
				cb();
			}
		});
	}

	// Trigger build of dist files then run revisioning
	gulp.task("build:dist-rev", ["styles", "scripts", "scripts-vendor"], () => {
		// By default, gulp would pick `assets/css` as the base,
		// so we need to set it explicitly:
		return gulp.src([
			`${config.assets.dest}/css/*.css`,
			`${config.assets.dest}/js/*.js`
		], {
			base: "public" // Set base to public so "assets" remains in the manifest URL
		})
			.pipe(rev()) // Append content hash to filenames
			.pipe(gulp.dest("public")) // write rev'd assets to build dir
			.pipe(removeOriginalAssets()) // remove original asset files
			.pipe(rev.manifest()) // create rev-manifest.json file
			.pipe(gulp.dest(config.assets.dest)); // write manifest file to build dir
	});

	// Build command to be run on the server after a deployment
	// This will get called every time we do the deploy task.
	gulp.task("build:production", (cb) => {
		runSequence(
			"clean",
			"modernizr",
			"build:dist-rev",
			cb
		);
	});

	// Build the production asset files.
	gulp.task("build:dist", (cb) => {
		runSequence(
			"clean",
			"build:dist-rev",
			cb
		);
	});

	// Build debug
	gulp.task("build:debug", ["set-debug", "styles", "scripts:debug", "scripts-vendor"]);

	// watch version of build which does clean
	gulp.task("build:watch", (cb) => {
		runSequence(
			"set-watch",
			"clean",
			"styles",
			"scripts-vendor",
			cb
		);
	});

	// default build (debug)
	gulp.task("build", () => {
		runSequence(
			"clean",
			"build:debug" // then run debug build so revisioning doesn't catch debug files
		);
	});

	// watch
	gulp.task("watch", ["build:watch", "watchify:debug"], () => {
		gulp.watch(config.assets.lint.less, ["styles"]);
	});
};
