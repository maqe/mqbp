module.exports = (gulp, config) => {
	const fs = require("fs");
	const modernizr = require("gulp-modernizr");

	// Generate custom modernizr public with limited set of tests
	// Needs to be run first time before dev
	gulp.task("modernizr", () => {
		// Get bower lib path for modernizr
		let bowerDirectory;
		try {
			// Read path from .bowerrc
			const bowerrc = JSON.parse(fs.readFileSync("./.bowerrc", "utf8"));
			bowerDirectory = bowerrc.directory;
		} catch (e) {
			// If .bowerrc is not exists, use default bower path
			bowerDirectory = "bower_components";
		}

		return gulp.src(config.assets.lint.js)
			.pipe(modernizr("modernizr-custom.js", {
				options: [
					"setClasses",
					"addTest",
					"testProp",
					"fnBind"
				],
				tests: config.options.modernizr,
				crawl: false
			}))
			.pipe(gulp.dest(bowerDirectory));
	});
};
