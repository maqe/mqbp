const gulp = require("gulp");
const fs = require("fs");
const path = require("path");
const argv = require("yargs").argv;
const globby = require("globby");

// Config file contains various app level variables
const configFile = process.env.GULP_BUILD_APP_CONFIG || argv.config || argv.c || "app-config.json";
const config = JSON.parse(fs.readFileSync(`./${configFile}`, "utf8"));

// Default config.
// Set debug to false so we default to dist/production build unless otherwise overwritten.
config.debug = false;
config.nodeEnv = "production";
config.watch = false;

// Load all Gulp tasks.
// config.tasks is array of possible locations
// Do a foreach look over each since requireDir expects single location
globby.sync(config.tasks).forEach((taskPath) => {
	console.log(`Loading task: ${path.basename(taskPath)}...`);
	require(path.resolve(taskPath))(gulp, config);
});

// Default gulp task (contained in "scripts/tasks/build.js").
gulp.task("default", ["build"]);
