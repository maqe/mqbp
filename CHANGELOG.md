## v1.4.2 (2018-02-05):

- Remove gulp-util since it's deprecated
- Put in fancy-log and plugin-error for replacement

## v1.4.1 (2018-01-29):

- Update dependencies
- Bump Stylint dependency to v8.4.0

## v1.4.0 (2018-01-15):

- Add "gulp deploy:ftp" task
- Update dependencies

## v1.3.0 (2017-10-12):

- You can now negate the task file in tasks config by using `!` at the beginning of a pattern
- Example config:
```
	"tasks": [
		"./node_modules/maqe-boilerplate/tasks/**/*.js",
		"!./node_modules/maqe-boilerplate/tasks/modernizr.js",
		"./tasks/**/*.js"
	]
```

## v1.2.0 (2017-10-06):

- Refactor and fix bugs
- Update dependencies
- Migrated to ESLint v4
- Add ability to create sourcemap for production asset files with --sourcemaps option

## v1.1.0 (2017-06-26):

- This version split MAQE's Stylelint rules into a separate extensible package (https://bitbucket.org/maqe/stylelint-config-maqe/).

### Breaking Changes:
- .stylelintrc will not symlinked out of this package anymore, instead it'll be copied out so that the file can be editable.
	- For that reason, you should remove .stylelintrc from your .gitignore
- .stylelintrc from this package will not override what you already have in your project.
- If you have .stylelintrc in your project before this version:
	- If it's a symlink, remove it.
	- If it's a custom file, change the "extends" value to "stylelint-config-maqe" and remove default rules in the file.
	- Then do the npm install.

## v1.0.4 (2017-06-19):

- Update the Stylelint with new rules

## v1.0.3 (2017-06-08):

- Update Babel to support spread operator

## v1.0.2 (2017-05-12):

- Add changelog

## v1.0.1 (2017-05-12):

- Add responsive mixin shorthand
- Update Stylint rules